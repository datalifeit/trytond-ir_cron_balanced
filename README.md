datalife_ir_cron_balanced
=========================

The ir_cron_balanced module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-ir_cron_balanced/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-ir_cron_balanced)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
